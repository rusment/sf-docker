FROM debian:stretch
WORKDIR /srv/app/
RUN apt-get update && apt-get install -y python3 python3-venv python3-pip postgresql-server-dev-9.6
RUN pip3 install Flask Psycopg2 ConfigParser
COPY web.py /srv/app/
COPY ./conf/web.conf /srv/app/conf/
ENTRYPOINT ["python3", "/srv/app/web.py"]
